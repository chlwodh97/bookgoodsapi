package com.jo.goodslistapi.controller;


import com.jo.goodslistapi.model.BookGoodsRequest;
import com.jo.goodslistapi.service.BookGoodsService;
import lombok.RequiredArgsConstructor;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;

@RestController
@RequiredArgsConstructor
@RequestMapping("/v1/book-goods")
public class BookGoodsController {

    private final BookGoodsService bookGoodsService;

    @PostMapping("new")
    public String setBookGoods(@RequestBody BookGoodsRequest request){
        bookGoodsService.setBookGoods(request);

        return "okok";
    }



}
