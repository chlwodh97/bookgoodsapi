package com.jo.goodslistapi;

import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;

@SpringBootApplication
public class GoodsListApiApplication {

	public static void main(String[] args) {
		SpringApplication.run(GoodsListApiApplication.class, args);
	}

}
