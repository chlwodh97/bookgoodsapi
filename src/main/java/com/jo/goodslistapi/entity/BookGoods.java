package com.jo.goodslistapi.entity;


import jakarta.persistence.*;
import lombok.Getter;
import lombok.Setter;

@Entity
@Getter
@Setter
public class BookGoods {

    @Id
    @GeneratedValue(strategy = GenerationType.IDENTITY)
    private Long id;

    @Column(nullable = false)
    private String imgTitle;

    @Column(nullable = false)
    private String name;

    @Column(nullable = false)
    private Double price;
}
