package com.jo.goodslistapi.model;


import lombok.Getter;
import lombok.Setter;

import java.util.List;

@Getter
@Setter
//무언가의 리스트리절트
public class ListResult<T> extends CommonResult {
    private List<T> list;
    private Long totalCount;
}
