package com.jo.goodslistapi.model;


import lombok.Getter;
import lombok.Setter;

@Getter
@Setter
public class BookGoodsRequest {


    private String imgTitle;

    private String name;

    private Double price;
}
