package com.jo.goodslistapi.model;


import lombok.Getter;
import lombok.Setter;

@Getter
@Setter
public class BookGoodsItem {
    private Long id;
    private String imgTitle;
    private String name;
    private Double price;
}
