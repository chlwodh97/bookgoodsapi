package com.jo.goodslistapi.repository;

import com.jo.goodslistapi.entity.BookGoods;
import org.springframework.data.jpa.repository.JpaRepository;

public interface BookGoodsRepository extends JpaRepository<BookGoods, Long> {
}
