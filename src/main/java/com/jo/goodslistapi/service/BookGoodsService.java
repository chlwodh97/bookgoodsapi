package com.jo.goodslistapi.service;


import com.jo.goodslistapi.entity.BookGoods;
import com.jo.goodslistapi.model.BookGoodsItem;
import com.jo.goodslistapi.model.BookGoodsRequest;
import com.jo.goodslistapi.repository.BookGoodsRepository;
import lombok.RequiredArgsConstructor;
import org.springframework.stereotype.Service;

import java.util.LinkedList;
import java.util.List;

@Service
@RequiredArgsConstructor
public class BookGoodsService {
    private final BookGoodsRepository bookGoodsRepository;


    public void setBookGoods(BookGoodsRequest request){

        BookGoods addData = new BookGoods();
        addData.setName(request.getName());
        addData.setImgTitle(request.getImgTitle());
        addData.setPrice(request.getPrice());

        bookGoodsRepository.save(addData);
    }

    public List<BookGoods> getBooksGoods() {
        //다가져와
        List<BookGoods> originList = bookGoodsRepository.findAll();

        List<BookGoodsItem> result = new LinkedList<>();
        for (BookGoods bookGoods : originList);
    `   `

    }




}
